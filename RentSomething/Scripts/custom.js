﻿$('document').ready(function () {
    var start_date = document.getElementById("startDate");
    var end_date = document.getElementById("endDate");

    var today = new Date();
    var tomorrow = new Date();
    var max_date = new Date();
    var end = new Date();

    tomorrow.setDate(today.getDate() + 1);
    tomorrow = tomorrow.toISOString().substr(0, 10);

    max_date.setDate(today.getDate() + 365);
    max_date = max_date.toISOString().substr(0, 10);

    today = today.toISOString().substr(0, 10);

    start_date.value = today;
    start_date.setAttribute("min", today);
    start_date.setAttribute("max", max_date);
        
    end.setDate(end.getDate() + 1);
    end = end.toISOString().substr(0, 10);
    end_date.value = end;
    end_date.setAttribute("max", end);
    end_date.setAttribute("min", tomorrow);

    calculatePrice();

    start_date.onchange = function () {
        var new_date = new Date(start_date.value);
        var next_day = new Date(start_date.value);

        new_date.setDate(new_date.getDate() + 30);
        new_date = new_date.toISOString().substr(0, 10);
        
        next_day.setDate(next_day.getDate() + 1);
        next_day = next_day.toISOString().substr(0, 10);

        end_date.value = next_day;
        end_date.setAttribute("max", new_date);
        end_date.setAttribute("min", next_day);

        calculatePrice();
    }
        
    end_date.onchange = function () {
        calculatePrice();
    }

});

function calculatePrice() {
    var start_date = document.getElementById("startDate");
    var end_date = document.getElementById("endDate");
    
    var sec = Math.abs(new Date(end_date.value) - new Date(start_date.value)) / 1000;
    var days = Math.floor(sec / 86400);

    var price = document.getElementById("pricePerDay").textContent;

    var total_price = price * days;

    document.getElementById("totalPrice").value = total_price;
}