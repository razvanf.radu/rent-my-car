﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentSomething.Models;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNet.Identity;

namespace RentSomething.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class CarsController : Controller
    {        
        public SqlConnection DbConnect()
        {
            string connectionString = @"Data Source=FIELD-RR\SQLEXPRESS;Initial Catalog=RentSomething;Integrated Security=True";
            SqlConnection conn = new SqlConnection(connectionString);

            return conn;
        }
        
        public SqlDataReader ExecuteQuerry(string cmd, SqlConnection conn)
        {
            SqlCommand sqlCmd = new SqlCommand(cmd, conn);
            SqlDataReader reader = sqlCmd.ExecuteReader();
            return reader;
        }
        
        // GET: Cars
        [Route("cars")]
        public ActionResult Index()
        {
            List<Car> cars = new List<Car>();
            cars = CreateCars();
            ViewBag.cars = cars;

            return View();
        }

        [HttpGet]
        [Route("cars/edit/{carId}")]
        public ActionResult Edit(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }
            

            Dictionary<int, string> fuel = GetFuelTypes();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> color = GetColors();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = color;

            return View(masina);
        }
        
        [HttpPost]
        [Route("cars/edit/{carId}")]
        public ActionResult Edit(int carId, string carModel, int regDate, int fuelType, int numSeats, int Colors, int Transmissions, int rentPrice )
        {
            string sqlCmd = "Update Cars Set car_model=@modelul, car_fabricatedAt=@regDate, car_fuel=@fuel, car_seats=@seats, car_color=@color, car_transmission=@trs, car_rent_price=@price, updatedAt=SYSDATETIME() where car_id=@id";
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);

            cmd.Parameters.AddWithValue("@modelul", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuel", fuelType);
            cmd.Parameters.AddWithValue("@seats", numSeats);
            cmd.Parameters.AddWithValue("@color", Colors);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@trs", Transmissions);
            cmd.Parameters.AddWithValue("@price", rentPrice);
            cmd.Parameters.AddWithValue("@id", carId);

            cmd.ExecuteNonQuery();

            return RedirectToAction("Index");         
        }

        [HttpGet]
        [Route("cars/add")]
        public ActionResult Add()
        {
            Car masina = new Car();
            Dictionary<int, string> fuel = GetFuelTypes();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> colors = GetColors();

            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = colors;

            return View(masina);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("cars/add")]
        public ActionResult Add(string carModel, int regDate, int fuelType, int numSeats, string carColor, int gearBox, int rentPrice)
        {
            SqlConnection conn = DbConnect();
            conn.Open();

            string sqlCmd = "INSERT INTO Cars (car_model, car_fuel, car_seats, car_color, car_transmission, car_rent_price, car_fabricatedAt, createdAt)" +
                            "VALUES (@model, @fuelType, @numSeats, @carColor, @trs, @rentPrice, @regDate, SYSDATETIME());";

            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@regDate", regDate);
            cmd.Parameters.AddWithValue("@fuelType", fuelType);
            cmd.Parameters.AddWithValue("@numSeats", numSeats);
            cmd.Parameters.AddWithValue("@carColor", carColor);
            cmd.Parameters.AddWithValue("@trs", gearBox);
            cmd.Parameters.AddWithValue("@rentPrice", rentPrice);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("cars/rent/{carId}")]
        [Authorize(Roles = "User")]
        public ActionResult Rent(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();
            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }

            return View(masina);
        }

        [HttpPost]
        [Route("cars/rent/{carId}")]
        [Authorize(Roles = "User")]
        public ActionResult Rent(int carId, string startDate, string endDate, int totalPrice)
        {
            SqlConnection conn = DbConnect();
            conn.Open();

            var userId = User.Identity.GetUserId();

            string sqlCmd = "Insert into Rentals(order_carId, order_custId, order_startedAt, order_endedAt, order_totalPrice, createdAt) Values (@carId, @custId, @startDate, @endDate, @totalPrice, SYSDATETIME())";
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);

            cmd.Parameters.AddWithValue("@carId", carId);
            cmd.Parameters.AddWithValue("@custId", userId);
            cmd.Parameters.AddWithValue("@startDate", startDate);
            cmd.Parameters.AddWithValue("@endDate", endDate);
            cmd.Parameters.AddWithValue("@totalPrice", totalPrice);

            cmd.ExecuteNonQuery();
            
            return RedirectToAction("Orders");           
        }

        [Authorize(Roles = "Admin")]
        [Route("cars/delete/{carId}")]
        public ActionResult Delete(int carId)
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            string SqlCmd = "DELETE from Cars WHERE car_id = " + carId;
            SqlCommand cmd = new SqlCommand(SqlCmd, conn);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("/");
        }

        public Dictionary<int, string> GetTransmissions()
        {
            SqlConnection conn = DbConnect();
            conn.Open();           
            SqlDataReader reader = ExecuteQuerry("Select * from Transmissions", conn);

            Dictionary<int, string> trs = new Dictionary<int, string>();

            while (reader.Read())
            {
                int trId = int.Parse(reader["tr_id"].ToString());
                string trVal = reader["tr_name"].ToString();

                trs[trId] = trVal;
            }

            return trs;
        }


        public Dictionary<int, string> GetFuelTypes()
        {
            SqlConnection conn = DbConnect();
            conn.Open();            
            SqlDataReader reader = ExecuteQuerry("Select * from FuelTypes", conn);

            Dictionary<int, string> fuel = new Dictionary<int, string>();

            while (reader.Read())
            {
                int fuelId = int.Parse(reader["fuel_id"].ToString());
                string fuelVal = reader["fuel_name"].ToString();

                fuel[fuelId] = fuelVal;
            }

            return fuel;
        }

        public Dictionary<int, string> GetColors()
        {
            SqlConnection conn = DbConnect();
            conn.Open();
            SqlDataReader reader = ExecuteQuerry("Select * from Colors", conn);

            Dictionary<int, string> color = new Dictionary<int, string>();

            while (reader.Read())
            {
                int colorId = int.Parse(reader["color_id"].ToString());
                string colorVal = reader["color_name"].ToString();

                color[colorId] = colorVal;
            }

            return color;
        }

        [Route("cars/view/{carId}")]
        public ActionResult View(int carId)
        {
            Car masina = new Car();
            List<Car> cars = CreateCars();

            if (carId == -1)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }
            ViewBag.image = "/Content/Images/car-" + carId + ".jpg";
            return View(masina);
        }


        public List<Car> CreateCars()
        {
            List<Car> cars = new List<Car>();
            SqlConnection conn = DbConnect();
            conn.Open();
            string sqlCmd = "Select Cars.*, FuelTypes.fuel_name, Transmissions.tr_name, Colors.color_name From Cars, FuelTypes, Transmissions, Colors WHERE Cars.car_fuel = FuelTypes.fuel_id AND Cars.car_transmission = Transmissions.tr_id AND Cars.car_color = Colors.color_id";
            SqlDataReader reader = ExecuteQuerry(sqlCmd, conn);

            while(reader.Read())
            {
                Car masina = new Car();

                masina.Id = int.Parse(reader["car_id"].ToString());
                masina.Model = reader["car_model"].ToString();
                masina.RegDate = int.Parse(reader["car_fabricatedAt"].ToString());
                masina.Fuel = reader["fuel_name"].ToString();
                masina.Seats = int.Parse(reader["car_seats"].ToString());
                masina.Color = reader["color_name"].ToString();
                masina.Gearbox = reader["tr_name"].ToString();
                masina.Price = int.Parse(reader["car_rent_price"].ToString());

                cars.Add(masina);
            }

            conn.Close();
            return cars;
        }
    }
}