﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RentSomething.Startup))]
namespace RentSomething
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
